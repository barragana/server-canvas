# Server Canvas

Server Canvas simulate a dashboard to manage server containers and applications.

Black boxes are new containers or cluster, which are able to run max two applications simultaneously.

## Setup
1. Clone repo
2. `yarn install`

## Running
1. `yarn start` -> localhost:3000
2. `yarn storybook` -> localhost:9009

## Testing
1. `yarn test`

## Components
### Styled Components
By [`styled-components`](styled-components.com) we get a better encapsulation, avoid writing multiple files `.js` and `.css`, get easier, more javascript way to share and extends components making it easier to see the style organisation.

### Atomic Design Approach
By using [`atomic desing`](http://bradfrost.com/blog/post/atomic-web-design/) approach we get well defined where each kind of component belongs to and how to organise them. It also helps reusability due the fact you build your view from smallest to biggest.

## Redux
### Ducks
By using [`ducks`](https://github.com/erikras/ducks-modular-redux) approach we avoid also to write many files for exemple: `constants.js`, `actions.js` and `reducers`. As usually they are not big and has same relation to each it invision that would make sense to group them in a single file, making navigation and accessibility easier.

### Saga
Despite that one of the main usability of [`redux-saga`](https://redux-saga.js.org/) is to handle asynchronous side effects like data fetching and impure things like accessing the browser cache, it also can be used to handle side effects of different reducers/store. Helpeing to separate some business logic from reducers. In our case, it handles side effects caused by `apps` to `servers` and vice-versa. So, `clurster` is responsible for applying the coordiation logic then trigger actions to update store accordingly.

### Selectors
By using selectors we extract some data we need from store easier. Also, we could memoized those selections and computations in case they don't change by using [`re-select`](https://github.com/reduxjs/reselect), improving performance by avoind re-computating selectors that does not change.

### Immutable
By using [`Immutable`](https://facebook.github.io/immutable-js/) we guarantee that state will be always an immutable object. It also facilitates some manipulation with arrays, object, etc once we get used to it.

## Storybook
[`Storybook`](http://storybook.org) brings component lib live and helps to develop components without depending on the rest of application. It allows to show case your components and also have some tests related to it. So, a good way to start creating components that later will be integrate to a page.

## Test
Usually use [`jest`](https://jestjs.io/) and [`enzyme`](https://airbnb.io/enzyme/docs/api/) for unit testing components and logic.

# Improvements
1. Add `re-select` for some selectors
2. Add integration tests between `actions` -> `saga` -> `store`. A kind of test that validates the whole flow, by taking a real(mocked) store as based and computating over it. The result of these kinds of tests are assertions for example that by dispaching an `ADD_APP_REQUESTED` at the and we would have that app added to the correct server in the store. 
 The ideal would be to write one test assertiong like that for each business logic scenario, by doing that we would assure behavior for integrating with page.
3. Styles improvements like adding svgs for `+` and `-`, and the animations delays for starting and destroying apps and servers
4. Simulate api requests for creating and destroying apps and servers


This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app), so any doubts about configuration check at the link.
