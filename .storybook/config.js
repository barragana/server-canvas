import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { ThemeProvider, injectGlobal } from 'styled-components';

import theme from '../src/components/themes/theme';
// Register decorator
addDecorator(story => {
  injectGlobal`
    html,
    body {
      background-color: ${theme.colors.black400};
    }

    * {
      box-sizing: border-box;
    }
  `;

  return (
    <ThemeProvider theme={theme}>
      {story()}
    </ThemeProvider>
  );
});

const req = require.context('../src', true, /\.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
