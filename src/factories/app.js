import moment from 'moment';
import { fromJS } from 'immutable';

function app({ id, uid, name, color, serverId, short, serverIndex }) {
  return fromJS({
    uid,
    id,
    name,
    serverId,
    startedAt: moment(),
    color,
    short,
    serverIndex,
  });
}

export default app;
