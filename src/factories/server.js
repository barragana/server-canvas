import { fromJS, List } from 'immutable';

export default function server(id) {
  return fromJS({
    id,
    apps: [],
  });
}

export function initServers() {
  return List(
    Array.from({ length: 4 }).map((_, index) => {
      return server(index + 1);
    }),
  );
}
