import { fromJS } from 'immutable';

import appsAvailable from '__mocks__/apps';

export const ADD_APP_REQUESTED = 'ADD_APP_REQUESTED';
export const ADD_APP_SUCCEED = 'ADD_APP_SUCCEED';
export const ADD_APP_FAILED = 'ADD_APP_SUCCEED';
export const REMOVE_APP_REQUESTED = 'REMOVE_APP_REQUESTED';
export const FINISH_REMOVE_APP = 'FINISH_REMOVE_APP';
export const UPDATE_APP = 'UPDATE_APP';

export function addAppRequested(app) {
  return {
    type: ADD_APP_REQUESTED,
    payload: app,
  };
}

export function addAppSucceed(app) {
  return {
    type: ADD_APP_SUCCEED,
    payload: { app },
  };
}

export function updateApp(app, index) {
  return {
    type: UPDATE_APP,
    payload: { app, index },
  };
}

export function removeAppRequested(app) {
  return {
    type: REMOVE_APP_REQUESTED,
    payload: { app },
  };
}

export function finishRemoveApp(index) {
  return {
    type: FINISH_REMOVE_APP,
    payload: { index },
  };
}

export const initialState = fromJS({
  isFetching: false,
  didInvalidate: false,
  error: '',
  list: [],
  appsAvailable,
  id: 0,
});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_APP_SUCCEED:
      return state.update('list', value => value.push(payload.app)).set('id', state.get('id') + 1);

    case ADD_APP_FAILED:
      return state.set('didInvalidate', true).set('error', payload.error);

    case FINISH_REMOVE_APP:
      return state.removeIn(['list', payload.index]);

    case UPDATE_APP:
      return state.mergeIn(['list', payload.index], payload.app);

    default:
      return state;
  }
};
