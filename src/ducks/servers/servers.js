import { fromJS } from 'immutable';

import server, { initServers } from '../../factories/server';

export const ADD_SERVER = 'ADD_SERVER';
export const DESTROY_SERVER_REQUESTED = 'DESTROY_SERVER_REQUESTED';
export const ADD_APP_TO_SERVER = 'ADD_APP_TO_SERVER';
export const REMOVE_APP_FROM_SERVER = 'REMOVE_APP_FROM_SERVER';
export const FINISH_DESTROY_SERVER = 'FINISH_DESTROY_SERVER';

export function addServer() {
  return {
    type: ADD_SERVER,
  };
}

export function destroyServerRequested() {
  return {
    type: DESTROY_SERVER_REQUESTED,
  };
}

export function finishDestroyServer() {
  return {
    type: FINISH_DESTROY_SERVER,
  };
}

export function addAppToServer(app, serverIndex) {
  return {
    type: ADD_APP_TO_SERVER,
    payload: { app, serverIndex },
  };
}

export function removeAppFromServer(app) {
  return {
    type: REMOVE_APP_FROM_SERVER,
    payload: { app },
  };
}

export const initialState = fromJS({
  isFetching: false,
  didInvalidate: false,
  error: '',
  list: initServers(),
  id: 4,
});

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_SERVER:
      return state.update('list', value => value.push(server(state.get('id') + 1))).update('id', value => (value += 1));

    case FINISH_DESTROY_SERVER:
      return state.update('list', value => value.pop());

    case ADD_APP_TO_SERVER:
      return state.updateIn(['list', payload.serverIndex, 'apps'], apps => apps.push(payload.app));

    case REMOVE_APP_FROM_SERVER: {
      const index = state
        .getIn(['list', payload.app.serverIndex, 'apps'])
        .findLastIndex(app => app.get('uid') === payload.app.uid);
      if (index === -1) {
        return state;
      }
      return state.updateIn(['list', payload.app.serverIndex, 'apps'], apps => apps.remove(index));
    }

    default:
      return state;
  }
};
