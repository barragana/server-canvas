import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { addServer, destroyServerRequested } from 'ducks/servers';
import { addAppRequested, removeAppRequested } from 'ducks/apps';
import { getServers } from 'selectors/servers';
import { getAvailableApps } from 'selectors/apps';

import Sidebar from 'components/organisms/Sidebar';
import Canvas from 'components/atoms/Canvas';
import Server from 'components/molecules/Server';
import { H3 } from 'components/atoms/Typography';

const StyledApp = styled('div')`
  background-color: ${props => props.theme.colors.black400};
  display: flex;
  justify-content: center;
  height: 100vh;
`;

const CanvasContainer = styled('div')`
  background-color: black;
  padding: 30px 20px 30px 25px;
  max-width: 325px;

  @media (min-width: 855px) {
    min-width: 605px;
    max-width: 605px;
  }
`;

const Title = styled(H3)`
  margin-left: 5px;
  margin-bottom: 15px;
  text-align: left;
`;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    };
    this.reRender = this.reRender.bind(this);
  }

  componentDidMount() {
    this.timeout = setInterval(this.reRender, 60000);
  }

  reRender() {
    this.setState({ date: new Date() });
  }

  componentWillUnmount() {
    clearInterval(this.timeout);
  }

  render() {
    const { apps, servers, ...props } = this.props;
    return (
      <StyledApp>
        <Sidebar options={apps} {...props} />
        <CanvasContainer>
          <Title>Server Canvas</Title>
          <Canvas>{servers.map(server => <Server key={server.id} apps={server.apps} />)}</Canvas>
        </CanvasContainer>
      </StyledApp>
    );
  }
}

App.propTypes = {
  apps: PropTypes.array,
  servers: PropTypes.array,
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onAddApp: addAppRequested,
      onRemoveApp: removeAppRequested,
      onAddServer: addServer,
      onDestroyServer: destroyServerRequested,
    },
    dispatch,
  );

const mapStateToProps = state => ({
  apps: getAvailableApps(state),
  servers: getServers(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
