export default [
  { name: 'Hadoop', uid: 1, color: 'purple', short: 'Hd' },
  { name: 'Rails', uid: 2, color: 'blue', short: 'Rl' },
  { name: 'Chronos', uid: 3, color: 'lightBlue', short: 'Ch' },
  { name: 'Storm', uid: 4, color: 'darkGreen', short: 'St' },
  { name: 'Spark', uid: 5, color: 'green', short: 'Sp' },
];
