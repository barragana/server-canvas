import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal, ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';

import registerServiceWorker from './registerServiceWorker';
import store from 'store';
import theme from 'components/themes/theme';
import App from 'scenes';

injectGlobal`
  html,
  body {
    margin: 0;
    padding: 0;
    background-color: ${theme.colors.black400};
    overflow: hidden;
  }

  * {
    box-sizing: border-box;
  }
`;

const ServerCanvas = () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>
);

ReactDOM.render(<ServerCanvas />, document.getElementById('root'));
registerServiceWorker();
