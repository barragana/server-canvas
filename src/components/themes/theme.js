const theme = {};

theme.colors = {
  black500: 'black',
  black400: '#191919',
  black300: '#323232',
  black200: '#4c4c4c',
  black100: '#666666',
  purple500: '#BA55D3',
  purple300: '#FF00FF',
  blue100: '#87CEFA',
  blue200: '#1E90FF',
  blue300: '#0000FF',
  blue500: '#4B0082',
  green100: '#7CFC00',
  green200: '#32CD32',
  green300: '#00FF7F',
  green500: '#2E8B57',
  grey: '#7f7f7f',
};

theme.borders = {
  l: '2px',
};

theme.palette = {
  main: 'transparent',
  white: 'white',
  grey: theme.colors.grey,
  lightBlack: theme.colors.black300,
  purple: linearGradient(theme.colors.purple500, theme.colors.purple300),
  green: linearGradient(theme.colors.green100, theme.colors.green200),
  darkGreen: linearGradient(theme.colors.green300, theme.colors.green500),
  blue: linearGradient(theme.colors.blue300, theme.colors.blue500),
  lightBlue: linearGradient(theme.colors.blue100, theme.colors.blue200),
};

function linearGradient(topColor, bottomColor) {
  return `linear-gradient(135deg, ${topColor}, ${bottomColor})`;
}

export default theme;
