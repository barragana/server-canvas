import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';

import ButtonWithLabel from './ButtonWithLabel';

storiesOf('Molecules/ButtonWithLabel', module)
  .add('White', () => <ButtonWithLabel onClick={action('clicked')} text="+" subText="Add server" palette="white" />)
  .add('Grey', () => <ButtonWithLabel onClick={action('clicked')} text="-" subText="Add server" palette="grey" />);
