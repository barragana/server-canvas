import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { SecondaryButton } from 'components/atoms/Button';
import { SubText } from 'components/atoms/Typography';

const StyledButton = styled(SecondaryButton)`
  display: inline-block;
  font-size: 50px;
  height: 40px;
  line-height: 0;
  width: 40px;
`;

const StyledDiv = styled('div')`
  align-items: center;
  display: flex;
  height: 65px;
  flex-flow: column;
  justify-content: space-between;
  width: 65px;
`;

const ButtonWithLabel = ({ text, subText, onClick, palette }) => (
  <StyledDiv>
    <StyledButton palette={palette} onClick={onClick}>
      {text}
    </StyledButton>
    <SubText palette={palette}>{subText}</SubText>
  </StyledDiv>
);

ButtonWithLabel.propTypes = {
  palette: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  subText: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ButtonWithLabel;
