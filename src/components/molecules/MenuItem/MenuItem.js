import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import BarDiv from 'components/atoms/BarDiv';
import { Text } from 'components/atoms/Typography';
import Button, { SecondaryButton } from 'components/atoms/Button';

const StyledMenuItem = styled(BarDiv)`
  align-items: center;
  display: flex;
  justify-content: space-between;
  padding: 10px 0 10px 20px;
  width: 100%;
`;

const cssButton = css`
  font-size: 30px;
  line-height: 0;
`;

const StyledButton = styled(Button)`
  ${cssButton};
  height: 24px;
  width: 24px;
`;

const StyledSecondaryButton = styled(SecondaryButton)`
  ${cssButton};
  height: 25px;
  width: 25px;
`;

const ActionsWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  margin-right: -10px;
  width: 60px;
`;

const MenuItem = ({ className, name, palette, onAdd, onRemove }) => (
  <StyledMenuItem className={className} palette={palette}>
    <Text>{name}</Text>
    <ActionsWrapper>
      <StyledSecondaryButton palette="grey" onClick={onRemove}>
        -
      </StyledSecondaryButton>
      <StyledButton palette={palette} onClick={onAdd}>
        +
      </StyledButton>
    </ActionsWrapper>
  </StyledMenuItem>
);

MenuItem.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  palette: PropTypes.string.isRequired,
  onAdd: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
};

MenuItem.defaultProps = {
  className: '',
};

export default MenuItem;
