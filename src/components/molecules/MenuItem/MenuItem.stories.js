import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';

import MenuItem from './MenuItem';

const Wrapper = storyFn => <div style={{ height: '100px', width: '150px', background: 'green' }}>{storyFn()}</div>;

storiesOf('Molecules/MenuItem', module)
  .addDecorator(Wrapper)
  .add('Default', () => (
    <MenuItem palette="green" name="Hadoop" onAdd={action('added')} onRemove={action('removed')} />
  ));
