import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { H3, Text, SubText } from 'components/atoms/Typography';
import Slot from 'components/atoms/Slot';

const ServerSlot = styled(Slot)`
  display: flex;
  height: 130px;
  margin: 5px;
  padding: 0;
  width: 130px;
`;

const AppSlot = styled(Slot)`
  flex: 1 1 auto;
  height: 130px;
`;

const Server = ({ apps }) => (
  <ServerSlot palette="lightBlack">
    {apps.map(({ id, color, short, name, startedAt }) => (
      <AppSlot key={id} palette={color}>
        <H3>{short}</H3>
        <Text>{name}</Text>
        <SubText>{startedAt.fromNow()}</SubText>
      </AppSlot>
    ))}
  </ServerSlot>
);

Server.propTypes = {
  apps: PropTypes.arrayOf(
    PropTypes.shape({
      short: PropTypes.string,
      name: PropTypes.string,
      startedAt: PropTypes.object,
      color: PropTypes.string,
    }),
  ),
};

Server.defaultProps = {
  apps: [],
};

export default Server;
