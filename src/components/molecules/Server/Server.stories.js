import React from 'react';
import moment from 'moment';
import { storiesOf } from '@storybook/react';

import Server from './Server';

storiesOf('Molecules/Server', module)
  .add('Default', () => <Server />)
  .add('With 1 App', () => (
    <Server apps={[{ id: 1, color: 'blue', short: 'Hd', name: 'Hadoop', startedAt: moment('2014-06-01T12:00:00Z') }]} />
  ))
  .add('With 2 Apps', () => (
    <Server
      apps={[
        { id: 1, color: 'blue', short: 'Hd', name: 'Hadoop', startedAt: moment('2014-06-01T12:00:00Z') },
        { id: 2, color: 'green', short: 'St', name: 'Storm', startedAt: moment('2014-06-01T12:00:00Z') },
      ]}
    />
  ));
