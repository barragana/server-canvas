import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button, { SecondaryButton } from './Button';

storiesOf('Atoms/Button', module)
  .add('Default', () => (
    <Button palette="purple" onClick={action('clicked')}>
      +
    </Button>
  ))
  .add('Secondary', () => (
    <SecondaryButton palette="grey" onClick={action('clicked')} disabled>
      -
    </SecondaryButton>
  ));
