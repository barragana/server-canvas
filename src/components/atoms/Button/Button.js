import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledButton = styled('button')`
  align-items: center;
  background: ${props => props.theme.palette[props.palette]};
  border: none;
  border-radius: 50%;
  color: white;
  display: flex;
  justify-content: center;
  padding: 0;
  position: relative;
  cursor: pointer;
  min-height: 20px;
  min-width: 20px;

  :focus,
  :active {
    outline: none;
  }
`;

const Text = styled('span')`
  position: relative;
  top: -2px;
`;

const Button = ({ children, ...props }) => (
  <StyledButton {...props}>
    <Text>{children}</Text>
  </StyledButton>
);

export const SecondaryButton = styled(Button)`
  background-color: transparent;
  border-width: 1px;
  border-style: solid;
  border-color: ${props => props.theme.palette[props.palette]};
  color: ${props => props.theme.palette[props.palette]};
`;

Button.propTypes = {
  palette: PropTypes.string,
};

Button.defaultProps = {
  palette: 'main',
};

export default Button;
