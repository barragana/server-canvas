import styled from 'styled-components';
import PropTypes from 'prop-types';

const Slot = styled('div')`
  background: ${props => props.theme.palette[props.palette]};
  padding: 10px;
`;

Slot.propTypes = {
  palette: PropTypes.string,
};

Slot.defaultProps = {
  palette: 'main',
  size: 'm',
  disabled: false,
};

export default Slot;
