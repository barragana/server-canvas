import React from 'react';

import { storiesOf } from '@storybook/react';

import Slot from './Slot';

const Content = () => <div style={{ height: '100px', width: '100px' }} />;

storiesOf('Atoms/Slot', module)
  .add('Blue', () => (
    <Slot palette="blue">
      <Content />
    </Slot>
  ))
  .add('Purple', () => (
    <Slot palette="purple">
      <Content />
    </Slot>
  ))
  .add('Light Blue', () => (
    <Slot palette="lightBlue">
      <Content />
    </Slot>
  ))
  .add('Green', () => (
    <Slot palette="green">
      <Content />
    </Slot>
  ))
  .add('Dark Green', () => (
    <Slot palette="darkGreen">
      <Content />
    </Slot>
  ))
  .add('Light Black', () => (
    <Slot palette="lightBlack">
      <Content />
    </Slot>
  ));
