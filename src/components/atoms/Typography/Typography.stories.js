import React from 'react';

import { storiesOf } from '@storybook/react';

import { H3, Text, SubText } from './Typography';

const Wrapper = ({ children }) => <div style={{ backgroundColor: 'white' }}>{children}</div>;

storiesOf('Atoms/Typography', module)
  .add('H3', () => <H3>Hd</H3>)
  .add('Text', () => <Text>Hadoop</Text>)
  .add('SubText', () => (
    <Wrapper>
      <SubText>Added 32 min ago</SubText>
    </Wrapper>
  ));
