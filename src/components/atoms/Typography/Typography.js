import styled from 'styled-components';

export const H3 = styled('h3')`
  color: white;
  margin: 5px 0;
  text-align: center;
`;

export const Text = styled('p')`
  color: white;
  font-size: 12px;
  font-weight: bold;
  margin: 10px 0;
  text-align: center;
`;

export const SubText = styled('p')`
  font-size: 12px;
  text-align: center;
  color: ${props => props.theme.palette[props.palette] || 'black'};
  margin: 0;
`;
