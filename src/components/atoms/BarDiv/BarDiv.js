import styled from 'styled-components';

const BarDiv = styled('div')`
  background-color: ${props => props.theme.colors.black400};
  color: white;
  left: 0;
  padding-left: 4px;
  position: relative;
  width: auto;
  &:before {
    background: ${props => props.theme.palette[props.palette]};
    bottom: 0;
    content: ' ';
    left: 0;
    position: absolute;
    top: 0;
    width: 4px;
  }
`;

export default BarDiv;
