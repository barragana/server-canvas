import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import BarDiv from './BarDiv';

const Wrapper = storyFn => <div style={{ height: '100px', width: '150px', background: 'green' }}>{storyFn()}</div>;

storiesOf('Atoms/BarDiv', module)
  .addDecorator(Wrapper)
  .add('Default', () => (
    <BarDiv palette="purple" onClick={action('clicked')}>
      <p>Loren loren loren</p>
    </BarDiv>
  ));
