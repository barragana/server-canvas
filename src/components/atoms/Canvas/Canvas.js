import styled from 'styled-components';

const Canvas = styled('div')`
  display: flex;
  flex-wrap: wrap;
`;

export default Canvas;
