import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';

import appsAvailable from '__mocks__/apps';

import Sidebar from './Sidebar';

const Wrapper = storyFn => <div style={{ height: '900px', width: '250px', background: 'darkgrey' }}>{storyFn()}</div>;

storiesOf('Organisms/Sidebar', module)
  .addDecorator(Wrapper)
  .add('Default', () => (
    <Sidebar
      onAddApp={action('addedApp')}
      onRemoveApp={action('removedApp')}
      onAddServer={action('addedServer')}
      onDestroyServer={action('removedServer')}
      options={appsAvailable}
    />
  ));
