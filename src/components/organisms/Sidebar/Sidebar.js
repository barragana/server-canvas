import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import MenuItem from 'components/molecules/MenuItem';
import ButtonWithLabel from 'components/molecules/ButtonWithLabel';
import { SubText } from 'components/atoms/Typography';

const StyledSidebar = styled('div')`
  align-items: center;
  background-color: ${props => props.theme.colors.black300};
  display: flex;
  flex-flow: column;
  flex-wrap: wrap;
  justify-content: center;
  height: 100%;
  position: relative;
  width: 250px;
`;

const Actions = styled('div')`
  display: flex;
  justify-content: space-between;
  margin: 40px;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
`;

const StyledSubText = styled(SubText)`
  margin-left: 20px;
  margin-bottom: 5px;
  text-align: left;
`;

const StyledMenuItem = styled(MenuItem)`
  border-bottom: ${props => `1px solid ${props.theme.colors.black300}`};
  :last-child {
    border-bottom: none;
  }
`;
const Menu = styled('div')`
  width: 100%;
`;

const Sidebar = ({ options, onAddApp, onRemoveApp, onAddServer, onDestroyServer }) => (
  <StyledSidebar>
    <Actions>
      <ButtonWithLabel text="+" subText="Add Server" palette="white" onClick={onAddServer} />
      <ButtonWithLabel text="-" subText="Destroy" palette="grey" onClick={onDestroyServer} />
    </Actions>
    <Menu>
      <StyledSubText palette="grey">Available Apps</StyledSubText>
      {options.map(option => (
        <StyledMenuItem
          key={option.uid}
          {...option}
          onAdd={() => onAddApp(option)}
          onRemove={() => onRemoveApp(option)}
          palette={option.color}
        />
      ))}
    </Menu>
  </StyledSidebar>
);

Sidebar.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      uid: PropTypes.number.isRequired,
      color: PropTypes.string.isRequired,
    }),
  ),
  onAddApp: PropTypes.func.isRequired,
  onRemoveApp: PropTypes.func.isRequired,
  onAddServer: PropTypes.func.isRequired,
  onDestroyServer: PropTypes.func.isRequired,
};

export default Sidebar;
