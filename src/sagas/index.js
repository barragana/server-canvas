import { all, fork } from 'redux-saga/effects';

import cluster from './cluster';

export default function* rootSaga() {
  yield all([fork(cluster)]);
}
