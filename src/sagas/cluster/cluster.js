import { call, put, takeLatest, select, all } from 'redux-saga/effects';

import { ADD_APP_REQUESTED, REMOVE_APP_REQUESTED, addAppSucceed, finishRemoveApp, updateApp } from 'ducks/apps';
import { DESTROY_SERVER_REQUESTED, addAppToServer, removeAppFromServer, finishDestroyServer } from 'ducks/servers';
import buildApp from 'factories/app';
import { getFirstServerAvailable, getLastServer } from 'selectors/servers';
import { getLastAppId, getLastAppByUid, getAppById } from 'selectors/apps';

export function* addNewAppToCluster({ payload: { uid, short, color, name } }) {
  const [serverIndex, serverAvailable] = yield select(getFirstServerAvailable);
  if (!serverAvailable) return;

  const lastAppId = yield select(getLastAppId);
  const app = yield call(buildApp, {
    id: lastAppId + 1,
    uid,
    color,
    name,
    short,
    serverIndex,
    serverId: serverAvailable.id,
  });
  yield all([put(addAppSucceed(app)), put(addAppToServer(app, serverIndex))]);
}

export function* removeAppFromCluster({ payload: { app: appInfo } }) {
  const [appIndex, app] = yield select(getLastAppByUid, appInfo.uid);
  if (!app) return;
  yield all([put(removeAppFromServer(app)), put(finishRemoveApp(appIndex))]);
}

export function* destroyServerFromCluster() {
  const server = yield select(getLastServer);
  yield put(finishDestroyServer());
  for (let app of server.apps) {
    const [serverIndex, serverAvailable] = yield select(getFirstServerAvailable);
    const [appIndex] = yield select(getAppById, app.id);
    if (!serverAvailable) {
      yield put(finishRemoveApp(appIndex));
    } else {
      const nextApp = buildApp({ ...app, serverIndex, serverId: serverAvailable.id });
      yield all([put(updateApp(nextApp, appIndex)), put(addAppToServer(nextApp, serverIndex))]);
    }
  }
}

function* watchCluster() {
  yield all([
    takeLatest(ADD_APP_REQUESTED, addNewAppToCluster),
    takeLatest(REMOVE_APP_REQUESTED, removeAppFromCluster),
    takeLatest(DESTROY_SERVER_REQUESTED, destroyServerFromCluster),
  ]);
}

export default watchCluster;
