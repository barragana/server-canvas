export const getAvailableApps = state => state.apps.get('appsAvailable').toJS();
export const getLastAppByUid = (state, uid) => {
  const app = state.apps.get('list').findLastEntry(app => app.get('uid') === uid);
  return (app && [app[0], app[1].toJS()]) || [-1, null];
};
export const getAppById = (state, id) => {
  const app = state.apps.get('list').findEntry(app => app.get('id') === id);
  return (app && [app[0], app[1].toJS()]) || [-1, null];
};
export const getLastAppId = state => state.apps.get('id');
