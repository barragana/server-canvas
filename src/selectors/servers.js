export const getServers = state => state.servers.get('list').toJS();

export const getLastServer = state =>
  state.servers
    .get('list')
    .last()
    .toJS();

export const getFirstServerAvailable = state => {
  const server = findServerAvailable(state, 0);
  return (server && [server[0], server[1].toJS()]) || [-1, null];
};

function findServerAvailable(state, length) {
  const server = state.servers.get('list').findEntry(server => server.get('apps').size === length);
  if (!server && length < 1) {
    return findServerAvailable(state, length + 1);
  }
  return server;
}
