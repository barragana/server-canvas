import React from 'react';
import initStoryshots from '@storybook/addon-storyshots';
import renderer from 'react-test-renderer';
import { ThemeProvider } from 'styled-components';
import 'jest-styled-components';

import theme from './components/themes/theme';

const styledSnapshot = ({ story, context }) => {
  const storyElement = <ThemeProvider theme={theme}>{story.render(context)}</ThemeProvider>;
  const tree = renderer.create(storyElement).toJSON();
  expect(tree).toMatchSnapshot();
};

initStoryshots({
  test: styledSnapshot,
});
